//格子的总数
var boxChildren = document.getElementsByClassName('box')[0].children;

//获取开始闪的元素
var start = document.getElementById('start');
var startFunction;
start.addEventListener('click',function(){
	startFunction = setInterval('threeNumber()',300);
});

//3个格变色
function threeNumber(){
	//把变了色的三个格变回橙色 
	for(var i=0;i<boxChildren.length;i++){
		boxChildren[i].style.backgroundColor='#ffa500';
	}

	//3个要变色格子的序号所组成的数组
	var numberArr = new Array();
	for(var i=0;i<3;i++){
		var newRandom = Math.ceil(Math.random()*9)-1; 
		if(numberArr.indexOf(newRandom)=='-1'){  //当数值没有存在数组中时，添加到数据中
			numberArr.push(newRandom);
		}else{ //如果已经存在数据中，则把i值减少，防止执行达到3次，当中出现出现相同值，不能获得3个数
			i--;
		}
	}

	//3种不同的颜色 
	var colorArr = new Array();
	
	for(var i=0;i<3;i++){
		var newColor = '#'; //循环六次。输出16进制的颜色
		for(var j=0;j<6;j++){
			var randomColor = Math.ceil( Math.random()*16)-1;
			switch(randomColor){
				case 10:
				newColor += 'a';
				break;

				case 11:
				newColor += 'b';
				break;

				case 12:
				newColor += 'c';
				break;

				case 13:
				newColor += 'd';
				break;

				case 14:
				newColor += 'e';
				break;

				case 15:
				newColor += 'f';
				break;

				default:
				newColor += randomColor;
				break;
			}
			if(newColor=='#ffa500'){  //判断是否等于原来的橙色，如果是，则重新再取一次值
				j=0;
			}
		}
		if(colorArr.indexOf(newColor)=='-1'){  //判断是否已存在数据中，如果没有，则添加，有就把i-1,再进行一次
			colorArr.push(newColor);
		}else{
			i--;
		}
	}
	
	for(var i=0;i<3;i++){  //为随机的3个空格添加不同的三种颜色
		boxChildren[ numberArr[i] ].style.backgroundColor = colorArr[i];
	}
}


//获取结束闪的元素
var end = document.getElementById('end');
end.addEventListener('click',function(){
	clearInterval(startFunction);
	for(var i=0;i<boxChildren.length;i++){
		boxChildren[i].style.backgroundColor='#ffa500';
	}
});

